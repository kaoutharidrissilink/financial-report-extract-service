package com.kyanja.financialreportextractservice;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinancialReportExtractServiceApplication implements CommandLineRunner {


    @Value("${spring.profiles.active}")
    private String environmentSetting;


    public static void main(String[] args) {
        SpringApplication.run(FinancialReportExtractServiceApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println(environmentSetting);
    }
}
